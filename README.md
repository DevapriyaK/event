EVENT PROCESSOR PROJECT - Onboarding excercise
Follow the below commands to clean & install required dependencies for the project
--------------------------------------------------------------------------------------------------------
To be executed from event-parent project path

-- mvn clean install
 
 To start up the project , navigate to event-app and execute the below command
 -------------------------------------------------------------------------------------------------
 
 -- spring-boot:run 
 
 The project by default runs with port number 8545, To change the port number
 change the application.properties file present in the event-app project and start the project again with the same above command.  