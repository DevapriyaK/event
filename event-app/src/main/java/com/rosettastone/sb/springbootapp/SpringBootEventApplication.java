package com.rosettastone.sb.springbootapp;

import java.util.concurrent.Executor;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableAsync; 
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.rosettastone.sb.mapper.UserMapper;
 
@SpringBootApplication
@MapperScan("com.rosettastone.sb.mapper")
@ImportResource("applicationContext.xml")
@ComponentScan("com.rs.learning.events.controller")
@EnableAsync
@EnableWebMvc
public class SpringBootEventApplication {
	
/*	@Autowired
	private UserMapper userMappper;*/
	
	    public static void main(String[] args) {
        SpringApplication.run(SpringBootEventApplication.class, args);
    }
	
/*	@Bean
	public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(10);
        taskExecutor.setThreadNamePrefix("EventExecutor-");
        taskExecutor.initialize();
        return taskExecutor;
	 }*/
}