package com.rosettastone.sb.event.entities;

public class TrackingModel {
	
	private int trackingID;
	private String message;
	private int totalProcessedCount;
	private int successEventsCount;
	private int failedEventsCount;

	public int getTrackingID() {
		return trackingID;
	}
	public int getTotalProcessedCount() {
		return totalProcessedCount;
	}
	public void setTotalProcessedCount(int totalProcessedCount) {
		this.totalProcessedCount = totalProcessedCount;
	}
	public void setTrackingID(int trackingID) {
		this.trackingID = trackingID;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public int getSuccessEventsCount() {
		return successEventsCount;
	}
	public void setSuccessEventsCount(int successEventsCount) {
		this.successEventsCount = successEventsCount;
	}
	public int getFailedEventsCount() {
		return failedEventsCount;
	}
	public void setFailedEventsCount(int failedEventsCount) {
		this.failedEventsCount = failedEventsCount;
	}

}
