package com.rosettastone.sb.event.entities;

import java.util.Date;

public class EventTracker {
	
	private int id;
	private int trackingId;
	private int userId;
	private int totalCountOfEvents;
	private int successCount;
	private int failedCount;
	private String createdAt;
	private Date updatedAt;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(int trackingId) {
		this.trackingId = trackingId;
	}
	public int getTotalCountOfEvents() {
		return totalCountOfEvents;
	}
	public void setTotalCountOfEvents(int totalCountOfEvents) {
		this.totalCountOfEvents = totalCountOfEvents;
	}
	public int getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}
	public int getFailedCount() {
		return failedCount;
	}
	public void setFailedCount(int failedCount) {
		this.failedCount = failedCount;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
	

}
