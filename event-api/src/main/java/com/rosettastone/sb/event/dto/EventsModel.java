package com.rosettastone.sb.event.dto;

import java.util.List;

import com.rosettastone.sb.event.entities.Event;

public class EventsModel {
	
	private List<Event> events;

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}
	

}
