package com.rosettastone.sb.event.dto;

public class ResponseModel {
	
	private int trackingID;
	private String message;
	public int getTrackingID() {
		return trackingID;
	}
	public void setTrackingID(int trackingID) {
		this.trackingID = trackingID;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}


}
