package com.rosettastone.sb.event.entities;

public class Event { 
	private String eventType;
	private int userId;
	private int eventSequence;
	private int userAgentId;
	private int clientId;
	private int productId;
	private int activityId;
	private int groupId;
	private String trackingId;
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getEventSequence() {
		return eventSequence;
	}
	public void setEventSequence(int eventSequence) {
		this.eventSequence = eventSequence;
	}
	public int getUserAgentId() {
		return userAgentId;
	}
	public void setUserAgentId(int userAgentId) {
		this.userAgentId = userAgentId;
	}
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public int getActivityId() {
		return activityId;
	}
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	
}