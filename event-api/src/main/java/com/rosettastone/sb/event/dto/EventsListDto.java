package com.rosettastone.sb.event.dto;

import java.util.List;

import com.rosettastone.sb.event.entities.Event;
import com.rosettastone.sb.event.entities.EventError;

public class EventsListDto {
	
	private List<Event> successEventsList;
	private List<EventError> failedEventsList;
	public List<Event> getSuccessEventsList() {
		return successEventsList;
	}
	public void setSuccessEventsList(List<Event> successEventsList) {
		this.successEventsList = successEventsList;
	}
	public List<EventError> getFailedEventsList() {
		return failedEventsList;
	}
	public void setFailedEventsList(List<EventError> failedEventsList) {
		this.failedEventsList = failedEventsList;
	}


}
