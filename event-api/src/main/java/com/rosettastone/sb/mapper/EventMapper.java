package com.rosettastone.sb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.rosettastone.sb.event.entities.Event;
import com.rosettastone.sb.event.entities.EventError;
import com.rosettastone.sb.event.entities.TrackingModel;

 
public interface EventMapper {
	
	void processSuccessEvents(@Param ("successEventsList") List<Event> successEventsList);
	void processFailedEvents(@Param ("failedEventsList") List<EventError> failedEventsList);
	TrackingModel getEventTrackingDetails(int trackingId);
	String validateEvent(Event event);
 
}
