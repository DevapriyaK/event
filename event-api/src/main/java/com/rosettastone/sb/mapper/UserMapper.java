package com.rosettastone.sb.mapper;

import com.rosettastone.sb.event.entities.User;

 
public interface UserMapper {
	
	User getUserById(int userId);
}
