package com.rosettastone.sb.mapper;

import com.rosettastone.sb.event.entities.EventTracker;
import com.rosettastone.sb.event.entities.TrackingModel;

 
public interface EventTrackerMapper {
	TrackingModel getEventTrackingDetails(EventTracker eventTracker); 
	void trackEventRequest(EventTracker eventTracker);
	void updateEventResponse(EventTracker eventTracker) ;
}
 