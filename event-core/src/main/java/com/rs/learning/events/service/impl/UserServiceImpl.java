package com.rs.learning.events.service.impl;

import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.rosettastone.sb.event.entities.User;
import com.rosettastone.sb.mapper.UserMapper;
import com.rs.learning.events.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;
	
	@Async
	public Future<User> findUserByIdASync(int userId) {
		User user = userMapper.getUserById(userId);
		return new AsyncResult<User>(user);	
	}
	
	public User getUserById(int userId) {
		return userMapper.getUserById(userId);		
	}

}
