package com.rs.learning.events.service.impl;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.rosettastone.sb.event.dto.EventsListDto;
import com.rosettastone.sb.event.dto.EventsModel;
import com.rosettastone.sb.event.entities.Event;
import com.rosettastone.sb.event.entities.EventError;
import com.rosettastone.sb.event.entities.EventTracker;
import com.rosettastone.sb.event.entities.User;
import com.rs.learning.events.service.EventProcessorService;
import com.rs.learning.events.service.EventTrackerService;
import com.rs.learning.events.service.FileHandlerService;
import com.rs.learning.events.utils.FunctionsUtil;



@Service
public class FileHandlerServiceImpl implements FileHandlerService { 

	static Logger logger = LoggerFactory.getLogger(FileHandlerServiceImpl.class);
	 EventsModel eventsModel = new EventsModel();

	 
	@Value("${upload.location}")
	private String SERVER_INPUT_LOCATION_FOLDER;
		
	@Autowired
	private EventProcessorService eventProcessorService; 
	
	@Autowired
	private EventTrackerService eventTrackerService;
	
	@Async
	public void writeToFile(User user,MultipartFile file,int trackingId) {			
		eventTrackerService.logEventRequest(user, trackingId);
		if (!file.isEmpty()) {
			String newFolderName = user.getUserId()+"_"+trackingId;
			String uploadedFileLocation = SERVER_INPUT_LOCATION_FOLDER + File.separator +newFolderName;
				if(! new File(uploadedFileLocation).exists())
                {
                    new File(uploadedFileLocation).mkdir();
                }
				String orgName = file.getOriginalFilename();
                String filePath = uploadedFileLocation + File.separator +orgName;                
            	File dest = new File(filePath);        		
              try {                	
					file.transferTo(dest);
				} catch (IllegalStateException | IOException e) {
					logger.error("Exception while handling file transfer" ,e);
				}
                if(dest.exists()){
                	readFile(user.getUserId(),trackingId);
                }
		}
	}	
	
	public EventsModel readFile(int usrId,int trackingId) {
		try {
			Files.walk(Paths.get(SERVER_INPUT_LOCATION_FOLDER + File.separator + usrId+"_"+trackingId))
				.filter(p -> p.toString().endsWith(".json"))
				.forEach(path -> {
			    if (Files.isRegularFile(path)) {
			    	eventsModel = readAsJsonObject(path.toFile(),trackingId,usrId);			    	
			    }
			});
		} catch (IOException e) {
				logger.error("IO exception file" ,e);
		}
		if(eventsModel != null && eventsModel.getEvents().size() > 0){
			int totalEventsCount = eventsModel.getEvents().size();
			int successEventsCount = 0;
			 EventsListDto eventsListDto = sanitizeEvent(eventsModel);
			 List<Event> successEventList = eventsListDto.getSuccessEventsList() ;
			 List<EventError> failedEventList = eventsListDto.getFailedEventsList() ;
			if(eventsListDto != null && successEventList.size() > 0 ){
				successEventsCount = successEventList .size(); 
				eventProcessorService.processSuccessEvents(successEventList );				
			}
			if(eventsListDto != null && failedEventList.size() > 0){				
				eventProcessorService.processFailedEvents(failedEventList);
			}
			updateProcessStatus(usrId,trackingId,totalEventsCount,successEventsCount);
		}
		return eventsModel;
	}

	public EventsModel readAsJsonObject(File file,int trackingId,int usrId){		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			Object object = jsonParser.parse(new FileReader(file));
			jsonObject = (JSONObject)object;
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		if(jsonObject != null){
			JSONArray eventsList = (JSONArray) jsonObject.get("events");			
			Iterator<JSONObject> eventIterator = eventsList.iterator();
			 List<Event> listOfEvents = new ArrayList<>();
			while(eventIterator.hasNext()){
				JSONObject jsonEvent = eventIterator.next();				
				listOfEvents.add(loadEventObject(jsonEvent,trackingId,usrId));				
			}
			eventsModel.setEvents(listOfEvents);
		}		
		return eventsModel;
	}
	
	private  Event loadEventObject(JSONObject jsonEvent,int trackingId,int usrId){
		Event event = new Event();
		event.setEventType((String)jsonEvent.get("eventType"));		
		event.setClientId(convertLongtoInt(jsonEvent.get("clientId")));
		event.setUserAgentId(convertLongtoInt(jsonEvent.get("userAgentId")));
		event.setProductId(convertLongtoInt(jsonEvent.get("productId")));
		event.setGroupId(convertLongtoInt(jsonEvent.get("groupId")));
		event.setActivityId(convertLongtoInt(jsonEvent.get("activityId")));
		event.setUserId(convertLongtoInt(jsonEvent.get("userId")));
		event.setEventSequence(convertLongtoInt(jsonEvent.get("eventSequence")));
		event.setTrackingId(String.valueOf(trackingId));
		return event;
	}	
	private int convertLongtoInt(Object jsonEvent){
		String value= "";
		if(jsonEvent instanceof Long){
			value = String.valueOf(jsonEvent);
		}
		return Integer.parseInt(value);
	}
	
	private EventsListDto sanitizeEvent(EventsModel eventsModel){
		EventsListDto  sanitizedList =  new EventsListDto();
		List<Event> successEventList = new ArrayList<>();
		List<EventError> failedEventList = new ArrayList<>();
		for(Event event : eventsModel.getEvents()){
			int eventPresentVal = Integer.parseInt(eventProcessorService.validateEvent(event));
			if(eventPresentVal != 1){
				// errorEvent = loadEventError(event);
				failedEventList.add( loadEventError(event));
			}else{
				successEventList.add(event);
			}			
		}
		sanitizedList.setSuccessEventsList(successEventList);
		sanitizedList.setFailedEventsList(failedEventList);
		return sanitizedList;
	}
	
	private void updateProcessStatus(int userId, int trackingId,int totalCount,int successCount ){
		EventTracker updateDetails = new EventTracker();
		updateDetails.setTotalCountOfEvents(totalCount);
		updateDetails.setSuccessCount(successCount);
		updateDetails.setFailedCount(totalCount - successCount );
		updateDetails.setUpdatedAt(FunctionsUtil.getCurrentDate());
		updateDetails.setUserId(userId);
		updateDetails.setTrackingId(trackingId);
		eventTrackerService.updateEventResponse(updateDetails); 
	}
	private  EventError loadEventError(Event event){
		EventError errorEvent = new EventError();
		errorEvent.setErrorId(30001);
		errorEvent.setErrorMsg("Invalid Event");
		errorEvent.setEventType(event.getEventType());
		errorEvent.setClientId(event.getClientId());
		errorEvent.setEventSequence(event.getEventSequence());
		errorEvent.setUserAgentId(event.getUserAgentId());
		errorEvent.setProductId(event.getProductId());
		errorEvent.setGroupId(event.getGroupId());
		errorEvent.setActivityId(event.getActivityId());
		errorEvent.setUserId(event.getUserId());
		errorEvent.setTrackingId(event.getTrackingId());
		return errorEvent;
	}
}