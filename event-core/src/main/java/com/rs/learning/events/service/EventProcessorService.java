package com.rs.learning.events.service;

import java.util.List;

import com.rosettastone.sb.event.entities.Event;
import com.rosettastone.sb.event.entities.EventError;


public interface EventProcessorService {
	
	String validateEvent(Event event);	
	void processSuccessEvents(List<Event> successEventsList);		
	void processFailedEvents(List<EventError> failedEventsList);
		


}
