package com.rs.learning.events.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class FunctionsUtil {
	static Logger logger = LoggerFactory.getLogger(FunctionsUtil.class);
	static SecureRandom prng;
	/**
	 * Initialize SecureRandom - PesudoRandom number generator (PRNG)
	 * This is a lengthy operation, to be done only upon
	 * initialization of the application
	 */
	static {
		try {
			prng = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	/**
	 * generate a non-negative random numbers added the Integer.MAX_VALUE . 
	 * this will generate integers in the range 0 (inclusive) to Integer.MAX_VALUE (exclusive).
	 *  In other words, it won't generate the value Integer.MAX_VALUE. 
	 */
	public static int generateTrackingId() {
		return prng.nextInt(Integer.MAX_VALUE);
	}

	public static Date getCurrentDate() {
		Calendar calobj = Calendar.getInstance();
		return calobj.getTime();
	}

	public static String getCurrentDateAsString() {
		Date date = new Date();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(	"yyyy/MM/dd HH:mm:ss");
		String currentTime = sdf.format(date);		
		return currentTime;
	}

}
