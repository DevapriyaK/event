package com.rs.learning.events.service;

import org.springframework.web.multipart.MultipartFile;

import com.rosettastone.sb.event.dto.EventsModel;
import com.rosettastone.sb.event.entities.User;


public interface FileHandlerService {
	
	void writeToFile(User user,MultipartFile file,int trackingId);
	EventsModel readFile(int userId,int trackingId);

}
