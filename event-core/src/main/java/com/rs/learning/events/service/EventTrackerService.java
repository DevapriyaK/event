package com.rs.learning.events.service;

import com.rosettastone.sb.event.entities.EventTracker;
import com.rosettastone.sb.event.entities.TrackingModel;
import com.rosettastone.sb.event.entities.User;


public interface EventTrackerService {
	
	TrackingModel getEventTrackingDetails(int trackingId,int userID);
	void logEventRequest(User user,int trackingId);
	void updateEventResponse(EventTracker eventTracker); 
}
