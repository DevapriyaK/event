package com.rs.learning.events.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rosettastone.sb.event.entities.EventTracker;
import com.rosettastone.sb.event.entities.TrackingModel;
import com.rosettastone.sb.event.entities.User;
import com.rosettastone.sb.mapper.EventTrackerMapper;
import com.rs.learning.events.service.EventTrackerService;
import com.rs.learning.events.utils.FunctionsUtil;


@Service
public class EventTrackerServiceImpl implements EventTrackerService {
	
	 
	@Autowired
	private EventTrackerMapper eventTrackerMapper;

	public TrackingModel getEventTrackingDetails(int trackingId,int userId) {
		EventTracker eventTracker = new EventTracker();
		eventTracker.setUserId(userId);
		eventTracker.setTrackingId(trackingId);
		return eventTrackerMapper.getEventTrackingDetails(eventTracker);
	}

	public void updateEventResponse(EventTracker eventTracker) {
		eventTrackerMapper.updateEventResponse(eventTracker);
	}

	public void logEventRequest(User user,int trackingId){
		EventTracker eventTracker = new EventTracker();
		eventTracker.setTrackingId(trackingId);
		eventTracker.setUserId(user.getUserId());
		eventTracker.setCreatedAt(FunctionsUtil.getCurrentDateAsString());
		eventTracker.setUpdatedAt(FunctionsUtil.getCurrentDate());		
		eventTrackerMapper.trackEventRequest(eventTracker);
	}
}
