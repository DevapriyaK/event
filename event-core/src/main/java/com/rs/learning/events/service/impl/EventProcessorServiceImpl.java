package com.rs.learning.events.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rosettastone.sb.event.entities.Event;
import com.rosettastone.sb.event.entities.EventError;
import com.rosettastone.sb.mapper.EventMapper;
import com.rs.learning.events.service.EventProcessorService;

@Service
public class EventProcessorServiceImpl implements EventProcessorService {

	@Autowired
	private EventMapper eventMapper;

	public void processSuccessEvents(List<Event> successEventsList) {
		eventMapper.processSuccessEvents(successEventsList);
	}

	public void processFailedEvents(List<EventError> failedEventsList) {
		eventMapper.processFailedEvents(failedEventsList);
	}

	public String validateEvent(Event event) {
		if(eventMapper.validateEvent(event) != null){
				return "1";
		}
		return "0";
		 
	}

}
