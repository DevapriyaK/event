package com.rs.learning.events.service;

import java.util.concurrent.Future;

import com.rosettastone.sb.event.entities.User;


public interface UserService {

	User getUserById(int userId);

	Future<User> findUserByIdASync(int userId);

}
