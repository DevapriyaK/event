package com.rs.learning.events.controller;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rosettastone.sb.event.dto.ResponseModel;
import com.rosettastone.sb.event.entities.TrackingModel;
import com.rosettastone.sb.event.entities.User;
import com.rs.learning.events.service.EventTrackerService;
import com.rs.learning.events.service.FileHandlerService;
import com.rs.learning.events.service.UserService;
import com.rs.learning.events.utils.FunctionsUtil;


@RestController
public class EventController {	
	
	static Logger logger = LoggerFactory.getLogger(EventController.class);

	@Autowired
	private UserService userService;
	
	@Autowired
	private FileHandlerService fileHandlerService;
	
	@Autowired
	private EventTrackerService eventTrackerService;	
	
	@RequestMapping(value="/{userId}",method=RequestMethod.POST,consumes= {"multipart/form-data"})	
    public @ResponseBody ResponseEntity<ResponseModel> uploadEventFile (@PathVariable("userId") @NotNull int id,@RequestPart("file") MultipartFile file) throws InterruptedException {
		ResponseModel responseModel = new ResponseModel();
    	User user = userService.getUserById(id);  
    	int trackingID = FunctionsUtil.generateTrackingId(); 
    	if(null == user){  
    		responseModel.setMessage("Invalid user");
    		responseModel.setTrackingID(0);      	
    		return new  ResponseEntity<ResponseModel>(responseModel,HttpStatus.OK);
        }    	 	
		responseModel.setMessage("Check the processing status with the trackingID");
		responseModel.setTrackingID(trackingID);
		if(file.getSize() > 10000){
			responseModel.setMessage("Submit files exceeding size 100KB for batch processing");
			responseModel.setTrackingID(0);
			return new  ResponseEntity<ResponseModel>(responseModel,HttpStatus.OK);
		}else{
			fileHandlerService.writeToFile(user,file,trackingID);
		}
    	return new  ResponseEntity<ResponseModel>(responseModel,HttpStatus.ACCEPTED);    	
	 }
	

	@RequestMapping(value="{userId}/status/{trackingId}",method=RequestMethod.GET)	
    public TrackingModel trackRequest(@PathVariable("userId") int userId,@PathVariable("trackingId") int trackingId) {
			TrackingModel trackingModel = new TrackingModel();
			User user = userService.getUserById(userId);
			if(user != null){
				trackingModel =  eventTrackerService.getEventTrackingDetails(trackingId,user.getUserId());
				if(trackingModel != null){
					trackingModel.setMessage("Events processing Completed");
				}else{
					trackingModel = new TrackingModel();
					trackingModel.setTrackingID(trackingId); 
					trackingModel.setMessage("Invalid tracking request.");
					return trackingModel;
				}
				if(trackingModel != null && trackingModel.getTotalProcessedCount() == 0){
					trackingModel.setTrackingID(trackingId);
					trackingModel.setMessage("Event Processing isin progress.. Please wait");
					return trackingModel;
				}
			}else{				
				trackingModel.setTrackingID(trackingId);
				trackingModel.setMessage("Invalid User");
				return trackingModel;
			}
			return trackingModel;		
    }
}